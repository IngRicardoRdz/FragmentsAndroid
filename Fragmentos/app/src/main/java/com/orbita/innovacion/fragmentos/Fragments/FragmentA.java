package com.orbita.innovacion.fragmentos.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orbita.innovacion.fragmentos.MandarDatosA;
import com.orbita.innovacion.fragmentos.R;

public class FragmentA extends Fragment {

    EditText name, pass;
    Button OK;
    TextView nam, pas;
    MandarDatosA mandarDatosA;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmenta, container, false);

        nam = (TextView) v.findViewById(R.id.txtNameA);
        pas = (TextView) v.findViewById(R.id.txtPassA);
        name = (EditText) v.findViewById(R.id.edtName);
        pass = (EditText) v.findViewById(R.id.edtPass);
        OK = (Button) v.findViewById(R.id.btnOK);

        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String n = name.getText().toString();
                String p = pass.getText().toString();

                mandarDatosA.enviarDatosA(n, p);
            }
        });

        return v;
    }

    public void Recibir(String n, String p){
        nam.setText("'" + n + "'");
        pas.setText("'" + p + "'");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mandarDatosA = (MandarDatosA) activity;
    }
}
